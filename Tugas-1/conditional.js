// 01. If-else
var nama;
var peran;

nama = prompt("Masukan nama: ");
peran = prompt("Masukan peran: ");
if (nama == "" && peran == "") {
    alert(`Nama harus diisi`);
} else if (nama == nama && peran == "") {
    alert(`Halo, ` + nama + ` Pilih peranmu untuk memulai game!`);
} else if (nama == nama && peran == "Penyihir") {
    alert(`Selamat datang di dunia Werewolf, ` + nama + `\n` + ` Halo, Penyihir` + nama + `, kamu dapat melihat siapa yang menjadi werewolf!`);
} else if (nama == nama && peran == "Guard") {
    alert(`Selamat datang di dunia Werewolf, ` + nama + `\n` + ` Halo, Guard` + nama + `, kamu akan membantu melindungi temanmu dari serangan werewolf!`);
} else if (nama == nama && peran == "Werewolf") {
    alert(`Selamat datang di dunia Werewolf, ` + nama + `\n` + ` Halo, Werewolf ` + nama + `, Kamu akan memakan mangsa setiap malam!`);
}

//=======================================================================================================================================================

//02. Switch Case
var tanggal;
var bulan;
var tahun;

tanggal = prompt(`Masukan tanggal: `);
bulan = prompt(`Masukan bulan: `);
tahun = prompt(`Masukan tahun: `);
switch (tanggal) {
    case "1": tanggal = "01"; break;
    case "2": tanggal = "02"; break;
    case "3": tanggal = "03"; break;
    case "4": tanggal = "04"; break;
    case "5": tanggal = "05"; break;
    case "6": tanggal = "06"; break;
    case "7": tanggal = "07"; break;
    case "8": tanggal = "08"; break;
    case "9": tanggal = "09"; break;
    case "10": tanggal = "10"; break;
    case "11": tanggal = "11"; break;
    case "12": tanggal = "12"; break;
    case "13": tanggal = "13"; break;
    case "14": tanggal = "14"; break;
    case "15": tanggal = "15"; break;
    case "16": tanggal = "16"; break;
    case "17": tanggal = "17"; break;
    case "18": tanggal = "18"; break;
    case "19": tanggal = "19"; break;
    case "20": tanggal = "20"; break;
    case "21": tanggal = "21"; break;
    case "22": tanggal = "22"; break;
    case "23": tanggal = "23"; break;
    case "24": tanggal = "24"; break;
    case "25": tanggal = "25"; break;
    case "26": tanggal = "26"; break;
    case "27": tanggal = "27"; break;
    case "28": tanggal = "28"; break;
    case "29": tanggal = "29"; break;
    case "30": tanggal = "30"; break;
    case "31": tanggal = "31"; break;
}
switch (bulan) {
    case "1": bulan = "Januari"; break;
    case "2": bulan = "Februari"; break;
    case "3": bulan = "Maret"; break;
    case "4": bulan = "April"; break;
    case "5": bulan = "Mei"; break;
    case "6": bulan = "Juli"; break;
    case "7": bulan = "Juni"; break;
    case "8": bulan = "Agustus"; break;
    case "9": bulan = "September"; break;
    case "10": bulan = "Oktober"; break;
    case "11": bulan = "November"; break;
    case "12": bulan = "Desember"; break;
}
var tampilTanggal = tanggal + ` ` + bulan + ` ` + tahun;
alert(tampilTanggal);

//=======================================================================================================================================================

// 05.Looping 
// a. While
let i = 1
while (i <= 20) {
    if (i % 2 == 0) {
        console.log(i + " -I Love Coding");
    }
    i++;
}
let j = 20
while (j >= 1) {
    if (j % 2 == 0) {
        console.log(j + " -Will Become a Mobile Developer");
    }
    j--;
}
//=======================================================================================================================================================

//b.for
for (let n = 1; n <= 20; n++) {
    if (n % 3 == 0) {
        console.log(n + " I Love Coding")
    } else if (n % 2 == 1) {
        console.log(n + " Teknik")
    } else if (n % 2 == 0) {
        console.log(n + " Informatika")
    }
}

//=======================================================================================================================================================

//c. Persegi Panjang
let s = '';
for (let i = 1; i <= 4; i++) {
    //kolom
    for (let j = 1; j <= 8; j++) {
        s += '#';
    }
    s += '\n';
}
console.log(s);

//=======================================================================================================================================================

//d. Segitiga
var s = '';
for (var i = 1; i <= 7; i++) {
    for (var j = 0; j < i; j++) {
        s += '#';
    }
    s += '\n';
}
console.log(s);

//=======================================================================================================================================================

//e.Papan Catur
var s = '';
for (var i = 1; i <= 8; i++) {
    if (i % 2 == 0) {
        for (var j = 1; j <= 8; j++) {
            if (j % 2 == 0) {
                s += '#';
            } else {
                s += ' ';
            }
        }

    } else {
        for (var j = 1; j <= 8; j++) {
            if (j % 2 == 0) {
                s += ' ';
            } else {
                s += '#';
            }
        }
    }
    s += '\n';
}
console.log(s);