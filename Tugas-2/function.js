//Function

//01
var teriak = () => { return ("Halo Humanika!"); }
console.log(teriak());
console.log("");

//02
let kalikan = (num1, num2) => { return num1 * num2 }
console.log(kalikan(12, 4));
console.log("");

//03
var introduce = (name, age, address, hobby) => {
    return `Nama saya ` + name + `, umur saya ` + age + ` tahun, ` + `alamat saya di ` + address + `, dan saya punya hobby yaitu ` + hobby + `!`
}
console.log(introduce("agus", 30, "Jln. Malioboro, Yogyakarta", "Gaming"));
