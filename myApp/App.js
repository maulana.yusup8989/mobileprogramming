import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import LandingPage from './components/LandingPage';
import Login from './components/Login';

export default class App extends Component {
    render() {
        return (
            <View>
                <LandingPage />
            </View >
        )
    }
};
