import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, } from 'react-native';

export default function LandingPage() {
  return (
    <View style={styles.canvas}>
      <View style={styles.container}>
        <View style={styles.header}>
          <Image style={styles.settings} source={require('../assets/settings.png')} />
          <Text style={styles.titleText}> DATA USER</Text>
        </View>
        <TouchableOpacity style={styles.btnSignin}
          onPress={() => window.location.href = 'Login.js'}>
          <Text style={styles.signin}>SIGN IN</Text>
        </TouchableOpacity>
        <Text style={styles.createAkun}>
          Create a New Account
        </Text>
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  canvas: {
    flex: 1,
    backgroundColor: '#000000',
    alignItems: 'center',
    justifyContent: 'center',
  },
  container: {
    height: 667,
    width: 375,
    backgroundColor: '#FFFF',
    alignItems: 'center',
  },
  header: {
    height: 560,
    width: 375,
    borderBottomLeftRadius: 15,
    borderBottomRightRadius: 15,
    backgroundColor: '#79AEF2',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 45,
  },
  settings: {
    height: 22,
    width: 22,
    position: 'absolute',
    right: 10,
    top: 15,
  },
  titleText: {
    fontSize: 40,
    fontFamily: 'sans-serif',
    color: 'white',
  },
  createAkun: {
    fontSize: 16,
    fontFamily: 'sans-serif',
    alignItems: 'center',
    justifyContent: 'center',
    color: '#BABABA',
    marginTop: 80
  },
  btnSignin: {
    backgroundColor: '#90A0F8',
    width: 212,
    height: 45,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: -120,
    borderColor: 'white',
    borderRadius: 10,
    borderWidth: 2,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
  },
  signin: {
    color: 'white',
    fontFamily: 'sans-serif',
  }
});
