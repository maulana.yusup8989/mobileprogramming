import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, SafeAreaView, TextInput } from 'react-native';

export default function Login() {

  const [text, onChangeText] = React.useState();
  const [Password, onChangePassword] = React.useState();

  return (
    <View style={styles.canvas}>
      <View style={styles.container}>
        <View style={styles.header}>
          <Image style={styles.settings} source={require('../assets/settings.png')} />
          <Text style={styles.titleText}>Sign In</Text>
          <View style={styles.login}>
            <Text style={styles.textLogin}>Username</Text>
            <SafeAreaView>
              <TextInput style={styles.input} onChangeText={onChangeText} value={text}>
              </TextInput>
            </SafeAreaView>
            <Text style={styles.textLogin}>Password</Text>
            <SafeAreaView>
              <TextInput style={styles.input} onChangePassword={onChangePassword} value={Password}>
              </TextInput>
              <TouchableOpacity style={styles.btnSignin}
                onPress={() => document.location.href = 'Login.js'}>
                <Text style={styles.signin}>SIGN IN</Text>
              </TouchableOpacity>
            </SafeAreaView>
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  canvas: {
    flex: 1,
    backgroundColor: '#000000',
    alignItems: 'center',
    justifyContent: 'center',
  },
  container: {
    height: 667,
    width: 375,
    backgroundColor: '#FFFF',
    borderRadius: 8,
  },
  header: {
    height: 225,
    width: 375,
    borderBottomLeftRadius: 45,
    borderBottomRightRadius: 200,
    backgroundColor: '#79AEF2',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 45,
  },
  settings: {
    height: 22,
    width: 22,
    position: 'absolute',
    right: 10,
    top: 15,
  },
  titleText: {
    fontSize: 36,
    fontFamily: 'sans-serif',
    color: 'white',
    position: 'absolute',
    top: 15,
    left: 20,
  },
  login: {
    width: 332,
    height: 398,
    backgroundColor: 'white',
    marginTop: 450,
    borderRadius: 8,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,

    elevation: 10,
    paddingTop: 71
  },
  textLogin: {
    fontFamily: 'sans-serif',
    fontSize: 16,
    paddingLeft: 25,
    color: '#5A5A5A'
  },
  input: {
    height: 45,
    width: 290,
    margin: 12,
    borderWidth: 1,
    borderColor: '#E5E5E5',
    paddingLeft: 10,
    marginLeft: 21
  },
  btnSignin: {
    backgroundColor: '#90A0F8',
    width: 212,
    height: 45,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: 'white',
    borderRadius: 10,
    marginLeft: 21,
    marginTop: 50,
    width: 290,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
    borderColor: 'white',
    borderRadius: 10,
    borderWidth: 2,
  },
  signin: {
    color: 'white',
    fontFamily: 'sans-serif',
  }

  // createAkun: {
  //   fontSize: 16,
  //   fontFamily: 'sans-serif',
  //   alignItems: 'center',
  //   justifyContent: 'center',
  //   color: '#BABABA',
  //   marginTop: 80
  // },
});
